#pragma once

#include <QQmlContext>
#include <QQmlEngine>
#include <QQueue>
#include <QSharedPointer>
#include <QVector>
#include "mqttdata.h"

namespace QMQTT
{
    class Client;
}

class QQuickWindow;
class MqttSettingsContextObject;
class MqttManager : public QObject
{
    Q_OBJECT
public:
    explicit MqttManager();
    ~MqttManager() Q_DECL_OVERRIDE;
    QVector<MqttData> clientData() const;
    void setClientData(const QVector<MqttData> &clientData);
    bool enabled() const;
    void setEnabled(bool enabled);
    void openSettingsDialog();
    void sendTestMessage(const char *captureCode, const char *time, const char *date,
                     const char *mode, const char *type, const char *bitrate,
                     const char *message, const char *label) const;
    void sendMessage(const char *captureCode, const char *time, const char *date,
                     const char *mode, const char *type, const char *bitrate,
                     const char *message, const char *label);
private:
    void setupWindow();
    QJsonObject createJson(const char *captureCode, const char *time, const char *date,
                           const char *mode, const char *type, const char *bitrate,
                           const char *message, const char *label) const;
    void sendMqttMessage(const QJsonObject &json) const;
    QVector<MqttData> m_clientData;
    QVector<QSharedPointer<QMQTT::Client>> m_clients;
    QQueue<QJsonObject> m_messageQueue;
    bool m_enabled;
    QQmlEngine m_engine;
    QQmlContext m_context;
    QScopedPointer<QQuickWindow> m_window;
    QScopedPointer<MqttSettingsContextObject> m_contextObject;
private slots:
    void mqttSettingsAccepted();
    void mqttSettingsRejected();

    friend QDataStream &operator<<(QDataStream &stream, const MqttManager &manager);
    friend QDataStream &operator>>(QDataStream &stream, MqttManager &manager);
};
QDataStream &operator<<(QDataStream &stream, const MqttManager &manager);
QDataStream &operator>>(QDataStream &stream, MqttManager &manager);
