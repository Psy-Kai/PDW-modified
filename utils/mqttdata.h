#pragma once

#include <QObject>
#include <QVector>
#include <qmqtt.h>

class MqttData : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVector<QString> topics READ topics WRITE setTopics NOTIFY topicsChanged)
    Q_PROPERTY(QString description READ description WRITE setDescription NOTIFY descriptionChanged)
    Q_PROPERTY(QString host READ host WRITE setHost NOTIFY hostChanged)
    Q_PROPERTY(qint32 port READ port WRITE setPort NOTIFY portChanged)
public:
    explicit MqttData();
    MqttData(const MqttData &other);
    MqttData(MqttData &&other);
    MqttData &operator=(const MqttData &other);
    MqttData &operator=(MqttData &&other);
    QVector<QString> topics() const;
    QString description() const;
    QString host() const;
    qint32 port() const;
    bool operator==(const MqttData &other) const;
    bool operator!=(const MqttData &other) const;
public slots:
    void setTopics(const QVector<QString> &topics);
    void setDescription(const QString &description);
    void setHost(const QString &host);
    void setPort(const qint32 &port);
signals:
    void topicsChanged(QVector<QString> topics);
    void descriptionChanged(QString description);
    void hostChanged(QString host);
    void portChanged(qint32 port);
private:
    QVector<QString> m_topics;
    QString m_description;
    QString m_host;
    qint32 m_port;

    friend QDataStream &operator<<(QDataStream &stream, const MqttData &mqttData);
    friend QDataStream &operator>>(QDataStream &stream, MqttData &mqttData);
};
QDataStream &operator<<(QDataStream &stream, const MqttData &mqttData);
QDataStream &operator>>(QDataStream &stream, MqttData &mqttData);
