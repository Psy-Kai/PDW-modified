#include "qtdebugging.h"

#include <QDir>
#include <QGlobalStatic>
#include <QStandardPaths>
#include <QTextStream>

Q_GLOBAL_STATIC(LogFile, kLogFile)

void pdwMessageHandler(QtMsgType messageType, const QMessageLogContext &logContext, const QString &message)
{
    LogFile::instance().write(qFormatLogMessage(messageType, logContext, message));
}

LogFile::LogFile() :
    m_logFile(QDir(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation)).
              filePath(QStringLiteral("qt-logfile.txt")))
{
    QDir().mkpath(QFileInfo(m_logFile).absolutePath());
    m_logFile.open(QIODevice::Append);
}

LogFile &LogFile::instance()
{
    return *kLogFile;
}

void LogFile::write(const QString &message)
{
    QTextStream stream(&m_logFile);
    stream << message << endl;
}
