#include "mqttmanager.h"

#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QQmlComponent>
#include <QQuickWindow>
#include <QTimer>
#include <qmqtt.h>
#include "../mqttsettingsmodel.h"

const QString kRequest = QStringLiteral("request");
const char kNewLine = '\n';

MqttManager::MqttManager() :
    m_context(m_engine.rootContext())
  , m_contextObject(new MqttSettingsContextObject)
{
    m_context.setContextObject(m_contextObject.data());
}

MqttManager::~MqttManager()
{}

QVector<MqttData> MqttManager::clientData() const
{
    return m_clientData;
}

void MqttManager::setClientData(const QVector<MqttData> &clientData)
{
    if (m_clientData == clientData)
        return;

    m_clientData = clientData;
    m_clients.resize(m_clientData.size());
    for (auto i = m_clients.size()-1; 0 <= i; --i) {
        auto &client = m_clients[i];
        const auto &clientData = m_clientData[i];
        if (client == Q_NULLPTR)
            client.reset(new QMQTT::Client);
        client->disconnect();
        QObject::connect(client.data(), &QMQTT::Client::error,
                         [client](const QMQTT::ClientError error) -> void {
            qWarning() << error;
            QTimer::singleShot(3000, client.data(), [client]() -> void { client->connectToHost(); });
        });
        QObject::connect(client.data(), &QMQTT::Client::connected,
                         [client, clientData]() -> void {
            for (const auto &topic : clientData.topics())
                client->subscribe(topic + QLatin1Char('/') + kRequest);
        });
        QObject::connect(client.data(), &QMQTT::Client::received,
                         [this, client, clientData](const QMQTT::Message &message) -> void {
            QMQTT::Message mqttMessage;
            auto topic = message.topic();
            topic.chop(kRequest.size()+1);
            mqttMessage.setTopic(topic);
            for (const auto &messageJson : m_messageQueue) {
                mqttMessage.setPayload(QJsonDocument(messageJson).toJson(QJsonDocument::Compact).append(kNewLine));
                client->publish(mqttMessage);
            }
        });
        client->setHostName(clientData.host());
        client->setPort(clientData.port());
        if (m_enabled)
            client->connectToHost();
        else
            client->disconnectFromHost();
    }
}

bool MqttManager::enabled() const
{
    return m_enabled;
}

void MqttManager::setEnabled(bool enable)
{
    if (m_enabled == enable)
        return;

    m_enabled = enable;
    if (m_enabled) {
        for (const auto &client : m_clients)
            client->connectToHost();
    } else {
        for (const auto &client : m_clients)
            client->disconnectFromHost();
    }
}

void MqttManager::openSettingsDialog()
{
    setupWindow();

    if (m_window == Q_NULLPTR)
        return;

    m_window->show();
}

void MqttManager::sendTestMessage(const char *captureCode, const char *time, const char *date,
                                  const char *mode, const char *type, const char *bitrate,
                                  const char *message, const char *label) const
{
    auto json = createJson(captureCode, time, date, mode, type, bitrate, message, label);
    sendMqttMessage(json);
}

void MqttManager::sendMessage(const char *captureCode, const char *time, const char *date,
                              const char *mode, const char *type, const char *bitrate,
                              const char *message, const char *label)
{
    auto json = createJson(captureCode, time, date, mode, type, bitrate, message, label);
    m_messageQueue.enqueue(json);
    if (1000 < m_messageQueue.size())
        m_messageQueue.dequeue();
    sendMqttMessage(json);
}

void MqttManager::setupWindow()
{
    if (m_window != Q_NULLPTR) {
        Q_ASSERT(m_contextObject != Q_NULLPTR);
        m_contextObject->mqttServerModel()->setClientData(m_clientData);
        m_contextObject->setMqttEnabled(m_enabled);
        return;
    }

    QQmlComponent component(&m_engine, QUrl(QStringLiteral("qrc:/MqttSettings.qml")));
    m_window.reset(qobject_cast<QQuickWindow*>(component.create(&m_context)));
    if (m_window == Q_NULLPTR) {
        qCritical() << qUtf8Printable(component.errorString());
    } else {
        QObject::connect(m_window.data(), SIGNAL(accepted()),
                         this, SLOT(mqttSettingsAccepted()));
        QObject::connect(m_window.data(), SIGNAL(rejected()),
                         this, SLOT(mqttSettingsRejected()));

        setupWindow();
    }
}

QJsonObject MqttManager::createJson(const char *captureCode, const char *time, const char *date,
                                    const char *mode, const char *type, const char *bitrate,
                                    const char *message, const char *label) const
{
    QJsonObject json;
    json["Vehicle"] = QString::fromUtf8(captureCode);
    json["Time"] = QString::fromUtf8(time);
    json["Date"] = QString::fromUtf8(date);
    json["Mode"] = QString::fromUtf8(mode);
    json["Message"] = QString::fromUtf8(message);
    return json;

    Q_UNUSED(type)
    Q_UNUSED(bitrate)
    Q_UNUSED(label)
}

void MqttManager::sendMqttMessage(const QJsonObject &json) const
{
    QMQTT::Message mqttMessage;
    mqttMessage.setPayload(QJsonDocument(json).toJson(QJsonDocument::Compact).append(kNewLine));
    Q_ASSERT(m_clientData.size() == m_clients.size());
    for (auto i = m_clients.size()-1; 0 <= i; --i) {
        const auto &clientData = m_clientData[i];
        const auto &client = m_clients[i];
        Q_ASSERT(client != Q_NULLPTR);
        Q_ASSERT(clientData.host() == client->hostName());
        Q_ASSERT(clientData.port() == client->port());
        for (const auto &topic : clientData.topics()) {
            mqttMessage.setTopic(topic);
            client->publish(mqttMessage);
        }
    }
}

void MqttManager::mqttSettingsAccepted()
{
    if (m_window == Q_NULLPTR)
        return;

    m_window->hide();
    Q_ASSERT(m_contextObject != Q_NULLPTR);
    setClientData(m_contextObject->mqttServerModel()->clientData());
    setEnabled(m_contextObject->mqttEnabled());
}

void MqttManager::mqttSettingsRejected()
{
    if (m_window == Q_NULLPTR)
        return;

    m_window->hide();
}

QDataStream &operator<<(QDataStream &stream, const MqttManager &manager)
{
    return stream << manager.m_enabled << manager.m_clientData;
}

QDataStream &operator>>(QDataStream &stream, MqttManager &manager)
{
    auto &&clientData = manager.clientData();
    stream >> manager.m_enabled >> clientData;
    manager.setClientData(clientData);
    return stream;
}
