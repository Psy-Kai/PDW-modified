#pragma once

#include <QFile>

void pdwMessageHandler(QtMsgType messageType, const QMessageLogContext &logContext,
                       const QString &message);

class LogFile
{
public:
    explicit LogFile();
    static LogFile &instance();
    void write(const QString &message);
private:
    QFile m_logFile;
};
