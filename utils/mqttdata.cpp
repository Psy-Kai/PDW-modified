#include "mqttdata.h"

#include <QDataStream>

MqttData::MqttData() :
    QObject()
  , m_port(-1)
{}

MqttData::MqttData(const MqttData &other) :
    QObject()
  , m_topics(other.m_topics)
  , m_description(other.m_description)
  , m_host(other.m_host)
  , m_port(other.m_port)
{}

MqttData::MqttData(MqttData &&other) :
    QObject()
  , m_topics(qMove(other.m_topics))
  , m_description(qMove(other.m_description))
  , m_host(qMove(other.m_host))
  , m_port(qMove(other.m_port))
{}

MqttData &MqttData::operator=(const MqttData &other)
{
    MqttData tmp(other);
    *this = qMove(tmp);
    return *this;
}

MqttData &MqttData::operator=(MqttData &&other)
{
    if (this != &other) {
        m_topics = qMove(other.m_topics);
        m_description = qMove(other.m_description);
        m_host = qMove(other.m_host);
        m_port = qMove(other.m_port);
    }

    return *this;
}

QVector<QString> MqttData::topics() const
{
    return m_topics;
}

void MqttData::setTopics(const QVector<QString> &topics)
{
    m_topics = topics;
}

QString MqttData::description() const
{
    return m_description;
}

void MqttData::setDescription(const QString &description)
{
    m_description = description;
}

QString MqttData::host() const
{
    return m_host;
}

void MqttData::setHost(const QString &host)
{
    m_host = host;
}

qint32 MqttData::port() const
{
    return m_port;
}

void MqttData::setPort(const qint32 &port)
{
    m_port = port;
}

bool MqttData::operator==(const MqttData &other) const
{
    return (m_topics == other.m_topics) &&
            (m_description == other.m_description) &&
            (m_host == other.m_host) &&
            (m_port == other.m_port);
}

bool MqttData::operator!=(const MqttData &other) const
{
    return !(*this == other);
}

QDataStream &operator<<(QDataStream &stream, const MqttData &mqttData)
{
    return stream << mqttData.m_host << mqttData.m_port
                  << mqttData.m_description << mqttData.m_topics;
}

QDataStream &operator>>(QDataStream &stream, MqttData &mqttData)
{
    return stream >> mqttData.m_host >> mqttData.m_port
                  >> mqttData.m_description >> mqttData.m_topics;
}
