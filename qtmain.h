#pragma once

#include <QObject>
#include <QScopedPointer>
#include "utils/mqttmanager.h"

class QtMain : public QObject
{
    Q_OBJECT
public:
    explicit QtMain();
    ~QtMain() Q_DECL_OVERRIDE;
    static QtMain *instance();
    const MqttManager &mqttManager() const;
    MqttManager &mqttManager();
    void readSettings();
    void writeSettings() const;
private:
    QScopedPointer<MqttManager> m_mqttManager;
};
