#pragma once

#include <QPointer>
#include <QStringListModel>
#include "utils/mqttdata.h"

class MqttConnectionSettingsTopicsModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(MqttData* mqttData READ mqttData WRITE setMqttData NOTIFY mqttDataChanged)
public:
    enum class Roles {
        Text = Qt::UserRole + 1
    };
    explicit MqttConnectionSettingsTopicsModel(QObject *parent = Q_NULLPTR);
    Q_INVOKABLE void append(const QString &item);
    Q_INVOKABLE void remove(int index);
    int rowCount(const QModelIndex &parent) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;
    MqttData *mqttData() const;
public slots:
    void setMqttData(MqttData *data);
signals:
    void mqttDataChanged(MqttData* mqttData);
private:
    bool inBounds(int index) const;
    QPointer<MqttData> m_data;
    MqttData* m_mqttData;
};
