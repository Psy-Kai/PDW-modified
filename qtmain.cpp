#include "qtmain.h"

#include <QDataStream>
#include <QFile>
#include <QStandardPaths>
#include "mqttconnectionsettingstopicsmodel.h"
#include "utils/qtdebugging.h"

const QString kQtConfig = QStringLiteral("QtConfig.conf");
QtMain *kMain = Q_NULLPTR;

QtMain::QtMain()
{
    if (kMain == Q_NULLPTR)
        kMain = this;
    qInstallMessageHandler(pdwMessageHandler);
    qSetMessagePattern(QStringLiteral("%{time} "
                                      "%{type}"
                                      "%{if-category} %{category}%{endif}: "
                                      "%{message}"));
    LogFile::instance();

    qmlRegisterType<MqttConnectionSettingsTopicsModel>("pdw.components", 1, 0,
                                                       "MqttConnectionSettingsTopicsModel");

    m_mqttManager.reset(new MqttManager);
}

QtMain::~QtMain()
{
    kMain = Q_NULLPTR;
}

QtMain *QtMain::instance()
{
    return kMain;
}

const MqttManager &QtMain::mqttManager() const
{
    return *m_mqttManager;
}

MqttManager &QtMain::mqttManager()
{
    return *m_mqttManager;
}

void QtMain::readSettings()
{
    QFile file(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation) +
               QLatin1Char('/') + kQtConfig);
    if (!file.open(QIODevice::ReadOnly))
        return;
    QDataStream stream(&file);
    stream >> *m_mqttManager;
}

void QtMain::writeSettings() const
{
    QFile file(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation) +
               QLatin1Char('/') + kQtConfig);
    if (!file.open(QIODevice::WriteOnly))
        return;
    file.resize(0);
    QDataStream stream(&file);
    stream << *m_mqttManager;
}
