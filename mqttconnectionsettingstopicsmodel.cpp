#include "mqttconnectionsettingstopicsmodel.h"

MqttConnectionSettingsTopicsModel::MqttConnectionSettingsTopicsModel(QObject *parent) :
    QAbstractListModel(parent)
{}

void MqttConnectionSettingsTopicsModel::append(const QString &item)
{
    if (m_data == Q_NULLPTR)
        return;
    auto &&topics = m_data->topics();
    beginInsertRows(QModelIndex(), topics.size(), topics.size());
    topics << item;
    m_data->setTopics(topics);
    endInsertRows();
}

void MqttConnectionSettingsTopicsModel::remove(int index)
{
    if (m_data == Q_NULLPTR)
        return;
    if (!inBounds(index))
        return;
    auto &&topics = m_data->topics();
    beginRemoveRows(QModelIndex(), index, index);
    topics.remove(index);
    m_data->setTopics(topics);
    endRemoveRows();
}

int MqttConnectionSettingsTopicsModel::rowCount(const QModelIndex &parent) const
{
    if (m_data == Q_NULLPTR)
        return -1;
    if (parent.isValid())
        return -1;

    return m_data->topics().size();
}

QVariant MqttConnectionSettingsTopicsModel::data(const QModelIndex &index, int role) const
{
    if (m_data == Q_NULLPTR)
        return QVariant();
    if (!index.isValid())
        return QVariant();
    if (index.parent().isValid())
        return QVariant();
    if (!inBounds(index.row()))
        return QVariant();

    switch (static_cast<Roles>(role)) {
    case Roles::Text:
        return m_data->topics()[index.row()];
    default:
        return QVariant();
    }
}

QHash<int, QByteArray> MqttConnectionSettingsTopicsModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[static_cast<int>(Roles::Text)] = "text";
    return roles;
}

MqttData *MqttConnectionSettingsTopicsModel::mqttData() const
{
    return m_data.data();
}

void MqttConnectionSettingsTopicsModel::setMqttData(MqttData *data)
{
    if (m_data == data)
        return;

    beginResetModel();
    m_data = data;
    endResetModel();
    emit mqttDataChanged(m_data);
}

bool MqttConnectionSettingsTopicsModel::inBounds(int index) const
{
    if (m_data == Q_NULLPTR)
        return false;
    return (0 <= index) && (index < m_data->topics().size());
}
