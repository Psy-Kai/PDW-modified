import qbs

CppApplication {
    id: root
    name: "PDW"
    version: "3.1.0"
    condition: true && qbs.targetOS.contains("windows")
    property bool useSmtp: false
    property bool useMqtt: true

    consoleApplication: false

    files: [
        "Flex.cpp",
        "Gfx.cpp",
        "Initapp.cpp",
        "Misc.cpp",
        "PDW.cpp",
        "Pocsag.cpp",
        "Rsrc.rc",
        "acars.cpp",
        "decode.cpp",
        "ermes.cpp",
        "helper_funcs.cpp",
        "language.cpp",
        "menu.cpp",
        "mobitex.cpp",
        "printer.cpp",
        "qml.qrc",
        "qtmain.cpp",
        "qtmain.h",
        "resrc1.h",
        "sigind.cpp",
        "sound_in.cpp",
        "toolbar.cpp",
    ]

    cpp.dynamicLibraries: [
        "gdi32",
        "comdlg32",
        "Comctl32",
        "Winmm",
    ]
    cpp.windowsApiCharacterSet: undefined

    Properties {
        condition: root.useSmtp
        cpp.defines: outer.concat(defines)

        property var defines: [
            "USE_SMTP"
        ]
        cpp.staticLibraries: outer.concat(staticLibraries)
        property var staticLibraries: [
            product.sourceDirectory + "/openssl-0.9.8l/out32/libeay32.lib",
            product.sourceDirectory + "/openssl-0.9.8l/out32/ssleay32.lib",
        ]
        cpp.dynamicLibraries: outer.concat(dynamicLibraries)
        property var dynamicLibraries: [
            "Ws2_32"
        ]
        cpp.includePaths: outer.concat(includePaths)
        property var includePaths: [
            product.sourceDirectory + "/openssl-0.9.8l/inc32"
        ]
    }

    Properties {
        condition: qbs.toolchain.contains("msvc")
        cpp.defines: [
            "-D_CRT_SECURE_NO_WARNINGS"
        ]
    }

    Group {
        name: "Headers"
        prefix: "Headers/"
        files: [
            "Resource.h",
            "SLICER.H",
            "acars.h",
            "decode.h",
            "ermes.h",
            "gfx.h",
            "helper_funcs.h",
            "initapp.h",
            "language.h",
            "menu.h",
            "misc.h",
            "mobitex.h",
            "pdw.h",
            "printer.h",
            "sigind.h",
            "sound_in.h",
            "toolbar.h",
        ]
    }

    Group {
        name: "utils"
        prefix: "utils/"
        files: [
            "Debug.cpp",
            "Ostype.cpp",
            "binary.cpp",
            "binary.h",
            "ipcnfg.h",
            "mqttdata.cpp",
            "mqttdata.h",
            "mqttmanager.cpp",
            "mqttmanager.h",
            "qtdebugging.cpp",
            "qtdebugging.h",
            "rs232.cpp",
            "rs232.h",
            "smtp.cpp",
            "smtp.h",
            "smtp_int.h",
        ]
    }

    Group {
        name: "models"
        files: [
            "mqttconnectionsettingstopicsmodel.cpp",
            "mqttconnectionsettingstopicsmodel.h",
            "mqttsettingsmodel.cpp",
            "mqttsettingsmodel.h",
        ]
    }

    Depends {
        name: "Qt"
        submodules: [
            "quick",
            "network"
        ]
    }
    Depends {
        name: "mqtt"
        condition: root.useMqtt
    }
}
