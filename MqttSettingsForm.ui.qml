import QtQuick 2.4
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

Pane {
    id: root
    width: 400
    height: 400
    property alias buttonTestMqtt: buttonTestMqtt
    property alias dialogButtonBox: dialogButtonBox
    property alias listView: listView
    property alias buttonRemove: buttonRemove
    property alias buttonEdit: buttonEdit
    property alias buttonAdd: buttonAdd
    property alias checkBox: checkBox
    property alias groupBox: groupBox

    ColumnLayout {
        id: columnLayout
        anchors.fill: parent

        CheckBox {
            id: checkBox
            text: qsTr("Mqtt")
        }

        GroupBox {
            id: groupBox
            Layout.fillHeight: true
            Layout.fillWidth: true
            enabled: checkBox.checked

            ColumnLayout {
                anchors.fill: parent

                Label {
                    id: label
                    text: qsTr("Server List:")
                }

                TextList {
                    id: listView
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                }

                RowLayout {
                    Layout.alignment: Qt.AlignRight | Qt.AlignVCenter

                    Button {
                        id: buttonAdd
                        text: qsTr("Add")
                    }

                    Button {
                        id: buttonEdit
                        text: qsTr("Edit")
                        enabled: listView.currentItem !== null
                    }

                    Button {
                        id: buttonRemove
                        text: qsTr("Remove")
                        enabled: listView.currentItem !== null
                    }
                }
            }
        }

        RowLayout {
            anchors.left: parent.left
            anchors.right: parent.right

            Button {
                id: buttonTestMqtt
                text: qsTr("Test")
                anchors.left: parent.left
                anchors.leftMargin: dialogButtonBox.padding
            }

            DialogButtonBox {
                id: dialogButtonBox
                anchors.right: parent.right
                standardButtons: DialogButtonBox.Ok | DialogButtonBox.Cancel
            }
        }
    }
}
