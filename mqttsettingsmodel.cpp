#include "mqttsettingsmodel.h"

#include <QDateTime>
#include "qtmain.h"

QObject *MqttSettingsModel::createData() const
{
    return new MqttData;
}

void MqttSettingsModel::append(const QVariant &obj)
{
    if (!isMqttData(obj))
        return;

    beginInsertRows(QModelIndex(), m_clientData.size(), m_clientData.size());
    m_clientData << *obj.value<MqttData*>();
    endInsertRows();
}

QObject *MqttSettingsModel::get(int index) const
{
    if (!inBounds(index))
        return Q_NULLPTR;

    return new MqttData(m_clientData[index]);
}

void MqttSettingsModel::set(int index, const QVariant &obj)
{
    if (!inBounds(index))
        return;
    if (!isMqttData(obj))
        return;

    m_clientData[index] = *obj.value<MqttData*>();
    emit dataChanged(createIndex(index, 1), createIndex(index, 1));
}

void MqttSettingsModel::remove(int index)
{
    if (!inBounds(index))
        return;

    beginRemoveRows(QModelIndex(), index, index);
    m_clientData.remove(index, 1);
    endRemoveRows();
}

int MqttSettingsModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return -1;

    return m_clientData.size();
}

QVariant MqttSettingsModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    if (index.parent().isValid())
        return QVariant();
    if (!inBounds(index.row()))
        return QVariant();

    switch (static_cast<Roles>(role)) {
    case Roles::Description:
        return m_clientData[index.row()].description();
    default:
        return QVariant();
    }
}

QHash<int, QByteArray> MqttSettingsModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[static_cast<int>(Roles::Description)] = "text";
    return roles;
}

QVector<MqttData> MqttSettingsModel::clientData() const
{
    return m_clientData;
}

void MqttSettingsModel::setClientData(const QVector<MqttData> &clientData)
{
    beginResetModel();
    m_clientData = clientData;
    endResetModel();
}

bool MqttSettingsModel::inBounds(int index) const
{
    return (0 <= index) && (index < m_clientData.size());
}

bool MqttSettingsModel::isMqttData(const QVariant &obj) const
{
    return obj.value<MqttData*>() != Q_NULLPTR;
}

MqttSettingsContextObject::MqttSettingsContextObject() :
    m_mqttEnabled(false)
  , m_mqttServerModel(new MqttSettingsModel)
{}

bool MqttSettingsContextObject::mqttEnabled() const
{
    return m_mqttEnabled;
}

MqttSettingsModel *MqttSettingsContextObject::mqttServerModel() const
{
    Q_ASSERT(m_mqttServerModel != Q_NULLPTR);
    return m_mqttServerModel.data();
}

/* see function "GetTestMail" in PDW.cpp */
QByteArray getTestMail(int line)
{
    switch (line) {
        case 1:
            return "012345678";
        case 2:
            return qPrintable(QTime::currentTime().toString(QStringLiteral("HH:mm")));
        case 3:
            return qPrintable(QDate::currentDate().toString(QStringLiteral("dd-MM-yyyy")));
        case 4:
            return "FLEX";
        case 5:
            return "ALPHA";
        case 6:
            return "1600";
        case 7:
            return "Message";
        case 8:
            return "Label";
        default:
            return "";
    }
}

void MqttSettingsContextObject::testMqtt() const
{
    QtMain::instance()->mqttManager().sendTestMessage(getTestMail(1).constData(),
                                                     getTestMail(2).constData(),
                                                     getTestMail(3).constData(),
                                                     getTestMail(4).constData(),
                                                     getTestMail(5).constData(),
                                                     getTestMail(6).constData(),
                                                     getTestMail(7).constData(),
                                                     getTestMail(8).constData());
}

void MqttSettingsContextObject::setMqttEnabled(bool enableMqtt)
{
    if (m_mqttEnabled == enableMqtt)
        return;

    m_mqttEnabled = enableMqtt;
    emit mqttEnabledChanged(m_mqttEnabled);
}
