import QtQuick 2.4
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

Pane {
    id: form
    width: 400
    height: 400
    property alias textFieldTopic: textFieldTopic
    property alias textFieldDescription: textFieldDescription
    property alias textFieldPort: textFieldPort
    property alias textFieldHost: textFieldHost
    property alias groupBox: groupBox
    property alias buttonRemove: buttonRemove
    property alias buttonAdd: buttonAdd
    property alias listView: listView
    property alias buttonEdit: buttonEdit
    property alias dialogButtonBox: dialogButtonBox

    ColumnLayout {
        id: columnLayout
        anchors.fill: parent

        GroupBox {
            id: groupBox
            property bool edit: false
            Layout.fillWidth: true
            title: qsTr("Connection Data")

            GridLayout {
                anchors.right: parent.right
                anchors.left: parent.left
                rows: 5
                columns: 2
                Label {
                    text: qsTr("Host")
                }

                Label {
                    text: qsTr("Port")
                }

                TextField {
                    id: textFieldHost
                    Layout.fillWidth: true
                    readOnly: !groupBox.edit
                }

                TextField {
                    id: textFieldPort
                    Layout.fillWidth: true
                    readOnly: !groupBox.edit
                }

                Label {
                    text: qsTr("Description")
                    Layout.columnSpan: 2
                }

                TextField {
                    id: textFieldDescription
                    Layout.columnSpan: 2
                    Layout.fillWidth: true
                    readOnly: !groupBox.edit
                }

                Button {
                    id: buttonEdit
                    text: groupBox.edit ? qsTr("Accept") : qsTr("Edit")
                    Layout.columnSpan: 2
                    Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                }
            }
        }

        Label {
            text: qsTr("Topics")
        }

        TextList {
            id: listView
            Layout.fillHeight: true
            Layout.fillWidth: true
        }

        RowLayout {
            width: 100
            height: 100

            TextField {
                id: textFieldTopic
                Layout.fillWidth: true
            }

            Button {
                id: buttonAdd
                text: qsTr("Add")
                enabled: 0 < textFieldTopic.length
            }

            Button {
                id: buttonRemove
                text: qsTr("Remove")
                enabled: listView.currentItem !== null
            }
        }

        DialogButtonBox {
            id: dialogButtonBox
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            standardButtons: DialogButtonBox.Ok | DialogButtonBox.Cancel
        }
    }
}
