import QtQuick 2.4
import QtQuick.Window 2.3

Window {
    id: root
    width: 640
    height: 480
    title: qsTr("Mqtt")
    visible: true
    flags: Qt.Dialog
    modality: Qt.WindowModal

    signal accepted
    signal rejected

    MqttSettingsForm {
        id: form

        function addMqttData() {
            connectionSettings.model = mqttServerModel.createData();
            connectionSettings.index = -1;
            connectionSettings.show()
        }

        function editMqttData(index) {
            var mqttData = mqttServerModel.get(index);
            console.log("editMqttData: " + mqttData);
            connectionSettings.model = mqttData;
            connectionSettings.index = index;
            connectionSettings.show()
        }

        function removeMqttData() {
            mqttServerModel.remove(listView.currentIndex);
        }

        anchors.fill: parent

        checkBox.checked: mqttEnabled
        checkBox.onCheckedChanged: mqttEnabled = form.checkBox.checked;
        listView.model: mqttServerModel

        dialogButtonBox.onAccepted: root.accepted();
        dialogButtonBox.onRejected: root.rejected();

        buttonAdd.onClicked: addMqttData()
        buttonEdit.onClicked: editMqttData(listView.currentIndex)
        buttonRemove.onClicked: removeMqttData()
        listView.onSelected: {
            if (0 <= index)
                editMqttData(index)
        }
        buttonTestMqtt.onClicked: testMqtt();
    }

    MqttConnectionSettings {
        id: connectionSettings

        onAccepted: {
            var index = connectionSettings.index;
            var model = connectionSettings.model;
            if (index < 0)
                mqttServerModel.append(model);
            else
                mqttServerModel.set(index, model)
            connectionSettings.hide();
        }
        onRejected: connectionSettings.hide();
    }
}
