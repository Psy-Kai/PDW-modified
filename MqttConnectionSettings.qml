import QtQuick 2.4
import QtQuick.Window 2.3
import pdw.components 1.0

Window {
    id: root
    width: 640
    height: 480
    title: qsTr("Mqtt Server Configuration")
    flags: Qt.Dialog
    modality: Qt.WindowModal

    property var model: null
    property int index: -1

    signal accepted
    signal rejected

    onIndexChanged: form.groupBox.edit = index < 0;

    MqttConnectionSettingsForm {
        id: form
        anchors.fill: parent

        listView.model: topicsModel

        buttonEdit.onClicked: groupBox.edit = !groupBox.edit

        textFieldHost.text: if ((model !== null) && (model !== undefined))
                                return model.host
        textFieldHost.onTextChanged: if((model !== null) && (model !== undefined))
                                         model.host = textFieldHost.text
        textFieldPort.text: if ((model !== null) && (model !== undefined))
                                return model.port
        textFieldPort.onTextChanged: if ((model !== null) && (model !== undefined))
                                         model.port = textFieldPort.text
        textFieldDescription.text: if ((model !== null) && (model !== undefined))
                                       return model.description
        textFieldDescription.onTextChanged: if ((model !== null) && (model !== undefined))
                                                model.description = textFieldDescription.text

        buttonAdd.onClicked: {
            topicsModel.append(textFieldTopic.text);
            textFieldTopic.clear();
        }
        buttonRemove.onClicked: topicsModel.remove(listView.currentIndex);

        dialogButtonBox.onAccepted: root.accepted();
        dialogButtonBox.onRejected: root.rejected();

        Component.onCompleted: form.groupBox.edit = root.index < 0;
    }

    MqttConnectionSettingsTopicsModel {
        id: topicsModel
        mqttData: model
    }
}
