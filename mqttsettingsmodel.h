#pragma once

#include <QAbstractListModel>
#include "utils/mqttdata.h"
#include <QStringListModel>

class MqttSettingsModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum class Roles {
        Description = Qt::UserRole+1
    };
    Q_INVOKABLE QObject *createData() const;
    Q_INVOKABLE void append(const QVariant &obj);
    Q_INVOKABLE QObject *get(int index) const;
    Q_INVOKABLE void set(int index, const QVariant &obj);
    Q_INVOKABLE void remove(int index);
    int rowCount(const QModelIndex &parent) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;
    QVector<MqttData> clientData() const;
    void setClientData(const QVector<MqttData> &clientData);
private:
    bool inBounds(int index) const;
    bool isMqttData(const QVariant &obj) const;
    QVector<MqttData> m_clientData;
};

class MqttSettingsContextObject : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool mqttEnabled READ mqttEnabled WRITE setMqttEnabled
               NOTIFY mqttEnabledChanged)
    Q_PROPERTY(MqttSettingsModel* mqttServerModel READ mqttServerModel)
public:
    explicit MqttSettingsContextObject();
    bool mqttEnabled() const;
    MqttSettingsModel* mqttServerModel() const;
    Q_INVOKABLE void testMqtt() const;
public slots:
    void setMqttEnabled(bool mqttEnabled);
signals:
    void mqttEnabledChanged(bool mqttEnabled);
private:
    bool m_mqttEnabled;
    QScopedPointer<MqttSettingsModel> m_mqttServerModel;
};
