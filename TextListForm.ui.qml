import QtQuick 2.4
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

Item {
    id: root
    property alias mouseArea: mouseArea
    property alias listView: listView

    ListView {
        id: listView
        clip: true
        anchors.fill: parent
        currentIndex: -1

        delegate: Control {
            anchors.right: parent.right
            anchors.left: parent.left
            padding: 5
            contentItem: Label {
                text: model.text
            }
        }

        ScrollBar.vertical: ScrollBar {
            policy: ScrollBar.AsNeeded
        }

        boundsBehavior: Flickable.StopAtBounds
    }

    MouseArea {
        id: mouseArea
        anchors.fill: listView
    }
}
