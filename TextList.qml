import QtQuick 2.7
import QtQuick.Controls 2.3

Frame {
    id: root
    property var model
    property int currentIndex: form.listView.currentIndex
    property var currentItem: form.listView.currentItem

    signal selected(var index)

    padding: 2

    TextListForm {
        id: form
        anchors.fill: parent

        listView.onCurrentIndexChanged: root.currentIndex = form.listView.currentIndex;
        listView.onCurrentItemChanged: root.currentItem = form.listView.currentItem;
        listView.model: root.model
        listView.highlight: Rectangle {
            color: root.enabled ? "lightblue" : root.palette.midlight
            border.color: root.enabled ? root.palette.highlight : root.palette.dark
        }

        mouseArea.onClicked: {
            var listView = form.listView;
            var index = listView.indexAt(mouse.x, mouse.y);
            listView.currentIndex = index;
            root.selected(listView.indexAt(mouse.x, mouse.y));
        }
    }

    Component.onCompleted: {
        var bg = root.background;
        if (bg.hasOwnProperty("color"))
            bg.color = "white";
    }
}
